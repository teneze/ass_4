#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <assert.h>

#define stat xv6_stat  // avoid clash with host struct stat
#include "types.h"
#include "fs.h"
#include "stat.h"
#include "param.h"
#include "mbr.h"

#ifndef static_assert
#define static_assert(a, b) do { switch (0) case 0: case (a): ; } while (0)
#endif

// Disk layout:
// [ boot block | sb block | log | inode blocks | free bit map | data blocks ]

int nbitmap = PRSIZE/(BSIZE*8) + 1;
int ninodeblocks = NINODES / IPB + 1;
int nlog = LOGSIZE;  
int nmeta;    // Number of meta blocks (boot, sb, nlog, inode, bitmap)
int nblocks;  // Number of data blocks

int fsfd;
struct superblock sb;
char zeroes[BSIZE];
uint freeinode = 1;
uint freeblock;

void balloc(int);
void wsectFixedAddress(uint, void*);
void wsect(uint, void*);
void winode(uint, struct dinode*);
void rinode(uint inum, struct dinode *ip);
void rsect(uint sec, void *buf);
uint ialloc(ushort type);
void iappend(uint inum, void *p, int n);

// convert to intel byte order
ushort
xshort(ushort x)
{
  ushort y;
  uchar *a = (uchar*)&y;
  a[0] = x;
  a[1] = x >> 8;
  return y;
}

uint
xint(uint x)
{
  uint y;
  uchar *a = (uchar*)&y;
  a[0] = x;
  a[1] = x >> 8;
  a[2] = x >> 16;
  a[3] = x >> 24;
  return y;
}

struct mbr mbr;
int currPartitionIndex;

int
main(int argc, char *argv[])
{
  int i, cc, fd;
  uint rootino, inum, off;
  struct dirent de;
  char buf[BSIZE];
  struct dinode din;
  int bytesRead;
  uint nextFreeBlockOfKernel = 1; // Indicates the offset of the next free block to put the kernel data.

  static_assert(sizeof(int) == 4, "Integers must be 4 bytes!");

  if(argc < 2){
    fprintf(stderr, "Usage: mkfs fs.img files...\n");
    exit(1);
  }

  assert((BSIZE % sizeof(struct dinode)) == 0);
  assert((BSIZE % sizeof(struct dirent)) == 0);

  fsfd = open(argv[1], O_RDWR|O_CREAT|O_TRUNC, 0666);
  if(fsfd < 0){
    perror(argv[1]);
    exit(1);
  }

  for(i = 0; i < FSSIZE; i++) //zero the whole goddamn image/disk
      wsect(i, zeroes);
  
  
  // TASK 4:
  // #################### UPDATE MBR TO BE BOOTBLOCK, UPDATE PARTITION TABLE, PUSH MBR INTO FS.IMG ####################
  
  // Insert bootblock - bootblock is the parameter at index 2 of mkfs as we can see in the Makefile
  if((fd = open(argv[2], O_RDWR)) < 0){
    perror(argv[2]);
    exit(1);
  }
  
  // We know that the size of bootblock is BSIZE, then we can read it all in a single shot
  memset(buf, 0, BSIZE);
  read(fd, buf, BSIZE);
  memmove(mbr.bootstrap, buf, BOOTSTRAP);
  mbr.magic[0] = buf[BSIZE-2];
  mbr.magic[1] = buf[BSIZE-1];
  close(fd);
  
    
  // Insert kernel - kernel is the parameter at index 1 of mkfs as we can see in the Makefile
  if((fd = open(argv[3], O_RDWR)) < 0){
    perror(argv[3]);
    exit(1);
  }

  memset(buf, 0, sizeof(buf));
  // Read the kernel data and insert it block by block to fs.img.
  while((bytesRead = read(fd, buf, sizeof(buf))) > 0){
    wsectFixedAddress(nextFreeBlockOfKernel, buf);
    nextFreeBlockOfKernel++;
    memset(buf, 0, sizeof(buf));
  }  
  printf("***************** nextFreeBlockOfKernel = %d **********************\n", nextFreeBlockOfKernel);
  close(fd);
  
  
  for(currPartitionIndex = 0; currPartitionIndex < NUM_OF_PARTITIONS; currPartitionIndex++) { //currPartitionIndex = current partition index
    struct dpartition* partition = &(mbr.partitions[currPartitionIndex]);
    partition->flags = PART_ALLOCATED; // TODO: should be bootable iff this partition include sh and init..........
    partition->type = FS_INODE; //TODO wut?
    partition->offset = nextFreeBlockOfKernel + currPartitionIndex*PRSIZE; 
    partition->size = PRSIZE;
  }
  
  for(currPartitionIndex = 0; currPartitionIndex < NUM_OF_PARTITIONS; currPartitionIndex++) {
   
    // 1 fs block = 1 disk sector
    // superblock + num of blocks for log + num of blocks for inodes + num of blocks for bitmap ( = metadata)
    nmeta = 1 + nlog + ninodeblocks + nbitmap;
    nblocks = PRSIZE - nmeta; // nblocks = num of data blocks
    sb.size = xint(PRSIZE);
    sb.nblocks = xint(nblocks);
    sb.ninodes = xint(NINODES);
    sb.nlog = xint(nlog);
    sb.logstart = xint(1); // instead of +2 (mbr, superblock) we only account for the current partition:ws +1 for superblock.
    sb.inodestart = xint(1+nlog);
    sb.bmapstart = xint(1+nlog+ninodeblocks);
    freeblock = nmeta;     // the first free block that we can allocate
    printf("nmeta %d (boot, super, log blocks %u inode blocks %u, bitmap blocks %u) blocks %d total %d\n",
	  nmeta, nlog, ninodeblocks, nbitmap, nblocks, PRSIZE);
    
    freeinode = 1; // In case we have more than 1 partition, freeinode increases each loop so we want to initialize it back to 1.
    
    // Write the superblock 
    memset(buf, 0, sizeof(buf));
    memmove(buf, &sb, sizeof(sb));
    wsect(0, buf);

    rootino = ialloc(T_DIR);
    assert(rootino == ROOTINO);

    bzero(&de, sizeof(de));
    de.inum = xshort(rootino);
    strcpy(de.name, ".");
    iappend(rootino, &de, sizeof(de));

    bzero(&de, sizeof(de));
    de.inum = xshort(rootino);
    strcpy(de.name, "..");
    iappend(rootino, &de, sizeof(de));

    int foundInit = 0, foundShell = 0; // used to determine whether the current partition is bootable (has init and shell)
    for(i = 4; i < argc; i++){
      assert(index(argv[i], '/') == 0);

      if((fd = open(argv[i], 0)) < 0){
	perror(argv[i]);
	exit(1);
      }
      
      // Skip leading _ in name when writing to file system.
      // The binaries are named _rm, _cat, etc. to keep the
      // build operating system from trying to execute them
      // in place of system binaries like rm and cat.
      int flag=0;
      if(argv[i][0] == '_'){
	++argv[i];
	flag=1;
      }

      if (strncmp(argv[i], "sh", 3) == 0) foundShell = 1;
      if (strncmp(argv[i], "init", 4) == 0) foundInit = 1;
      if (foundShell && foundInit) {
	mbr.partitions[currPartitionIndex].flags |= PART_BOOTABLE;
      }

      inum = ialloc(T_FILE);

      bzero(&de, sizeof(de));
      de.inum = xshort(inum);
      strncpy(de.name, argv[i], DIRSIZ);
      iappend(rootino, &de, sizeof(de));
      
      if(flag)
	--argv[i];

      while((cc = read(fd, buf, sizeof(buf))) > 0)
	iappend(inum, buf, cc);

      close(fd);
    }
    

    // fix size of root inode dir
    rinode(rootino, &din);
    off = xint(din.size);
    off = ((off/BSIZE) + 1) * BSIZE;
    din.size = xint(off);
    winode(rootino, &din);

    balloc(freeblock);
  }

  // Push mbr into fs.img in the 0-block
  memset(buf, 0, sizeof(buf));
  memmove(buf, &mbr, sizeof(mbr));
  wsectFixedAddress(0, buf);
  
  exit(0);
}


// Writes a block to the disk to the exact given block (no relativity)
void
wsectFixedAddress(uint sec, void *buf)
{
  if(lseek(fsfd, sec * BSIZE, 0) != sec * BSIZE){
    perror("lseek");
    exit(1);
  }
  if(write(fsfd, buf, BSIZE) != BSIZE){
    perror("write");
    exit(1);
  }
}


// Writes a block to the disk, relative to the current partition//TODO***: currently the behavior for block 0 is different because of the mbr.
void
wsect(uint sec, void *buf)
{
  int offset = (mbr.partitions[currPartitionIndex].offset + sec) * BSIZE;
  if(lseek(fsfd, offset, 0) != offset){
    perror("lseek");
    exit(1);
  }
  if(write(fsfd, buf, BSIZE) != BSIZE){
    perror("write");
    exit(1);
  }
}

// Writes an inode struct from the RAM into the disk (its dinode struct).
void
winode(uint inum, struct dinode *ip)
{
  char buf[BSIZE];
  uint bn;
  struct dinode *dip;

  bn = IBLOCK(inum, sb);
  rsect(bn, buf);
  dip = ((struct dinode*)buf) + (inum % IPB);
  *dip = *ip;
  wsect(bn, buf);
}

// Reads a d-inode with the given inum from the disk for the given partition number, and puts a pointer to it in ip.
void
rinode(uint inum, struct dinode *ip)
{
  char buf[BSIZE];
  uint bn;
  struct dinode *dip;

  bn = IBLOCK(inum, sb);
  rsect(bn, buf);
  dip = ((struct dinode*)buf) + (inum % IPB);
  *ip = *dip;
}

// Reads a block from address uint in the current filesystem (how do we know which fs?) into buf. //TODO: partitions?
void
rsect(uint sec, void *buf)
{
  int offset = (mbr.partitions[currPartitionIndex].offset + sec) * BSIZE;
  if(lseek(fsfd, offset, 0) != offset){
    perror("lseek");
    exit(1);
  }
  if(read(fsfd, buf, BSIZE) != BSIZE){
    perror("read");
    exit(1);
  }
}

// Allocates an inode and returns its inum. Stores it on disk (and in RAM?). //TODO what does sbNum matter?
uint
ialloc(ushort type)
{
  uint inum = freeinode++;
  struct dinode din;

  bzero(&din, sizeof(din));
  din.type = xshort(type);
  din.nlink = xshort(1);
  din.size = xint(0);
  winode(inum, &din);
  return inum;
}

// TODO ??
// Allocates a block and marks it as used in the bitmap?
void
balloc(int used)
{
  uchar buf[BSIZE];
  int i;

  printf("balloc: first %d blocks have been allocated\n", used);
  assert(used < BSIZE*8);
  bzero(buf, BSIZE);
  for(i = 0; i < used; i++){
    buf[i/8] = buf[i/8] | (0x1 << (i%8));
  }
  printf("balloc: write bitmap block at sector %d\n", sb.bmapstart);
  wsect(sb.bmapstart, buf);
}

#define min(a, b) ((a) < (b) ? (a) : (b))

void
iappend(uint inum, void *xp, int n)
{
  char *p = (char*)xp;
  uint fbn, off, n1;
  struct dinode din;
  char buf[BSIZE];
  uint indirect[NINDIRECT];
  uint x;

  rinode(inum, &din);
  off = xint(din.size);
  // printf("append inum %d at off %d sz %d\n", inum, off, n);
  while(n > 0){
    fbn = off / BSIZE;
    assert(fbn < MAXFILE);
    if(fbn < NDIRECT){
      if(xint(din.addrs[fbn]) == 0){
        din.addrs[fbn] = xint(freeblock++);
      }
      x = xint(din.addrs[fbn]);
    } else {
      if(xint(din.addrs[NDIRECT]) == 0){
        din.addrs[NDIRECT] = xint(freeblock++);
      }
      rsect(xint(din.addrs[NDIRECT]), (char*)indirect);
      if(indirect[fbn - NDIRECT] == 0){
        indirect[fbn - NDIRECT] = xint(freeblock++);
        wsect(xint(din.addrs[NDIRECT]), (char*)indirect);
      }
      x = xint(indirect[fbn-NDIRECT]);
    }
    n1 = min(n, (fbn + 1) * BSIZE - off);
    rsect(x, buf);
    bcopy(p, buf + off - (fbn * BSIZE), n1);
    wsect(x, buf);
    n -= n1;
    off += n1;
    p += n1;
  }
  din.size = xint(off);
  winode(inum, &din);
}